<?php

/**
 * This file is part of the Webm package
 *
 * For the full copyright and license information,
 * view the LICENSE file that was distributed with this source code.
 */

use Kiririn\Webm\Webm;

/**
 * Class WebmTest
 */
class WebmTest extends \PHPUnit_Framework_TestCase {

    /**
     * @return \Kiririn\Webm\Webm
     */
    public function testWebmInfo() {
        $webm = new Webm('./tests/haha.webm');
        $this->assertEquals(400, $webm->getWidth());
        $this->assertEquals(400, $webm->getHeight());
        $this->assertEquals(1384, $webm->getDuration());
        return $webm;
    }


    /**
     * @depends testWebmInfo
     *
     * @param \Kiririn\Webm\Webm $webm
     * @return \Kiririn\Webm\Webm
     * @throws Exception
     */
    public function testThumbnailInfo(Webm $webm) {
        $webm->thumbnail(200, 200);
        $this->assertEquals(200, $webm->getThumbWidth());
        $this->assertEquals(200, $webm->getThumbHeight());
        return $webm;
    }


    /**
     * @depends testThumbnailInfo
     *
     * @param \Kiririn\Webm\Webm $webm
     * @return \Kiririn\Webm\Webm
     * @throws Exception
     */
    public function testCreateThumbnail(Webm $webm) {
        $webm->save('./tests', 'thumb.jpeg');
        $this->assertFileExists('./tests/thumb.jpeg');
        //cleanup
        unlink('./tests/thumb.jpeg');
        return $webm;
    }


    /**
     * @throws Exception
     */
    public function testCreateWithoutExt() {
        $webm = new Webm('./tests/haha.webm');
        $webm->thumbnail(200, 200);
        $webm->save('./tests', 'thumb');
        $this->assertFileExists('./tests/thumb.jpeg');
        //cleanup
        unlink('./tests/thumb.jpeg');
    }

}