<?php

/**
 * This file is part of the Webm package
 *
 * For the full copyright and license information,
 * view the LICENSE file that was distributed with this source code.
 */

$loader = require __DIR__ . '/../vendor/autoload.php';
$loader->addPsr4('src\Tests\\', __DIR__);