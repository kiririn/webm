<?php

/**
 * This file is part of the Webm package
 *
 * For the full copyright and license information,
 * view the LICENSE file that was distributed with this source code.
 */

namespace Kiririn\Webm\Exception;

class UnsatisfiedDependencyException extends \RuntimeException {}