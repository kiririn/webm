<?php

/**
 * This file is part of the Webm package
 *
 * For the full copyright and license information,
 * view the LICENSE file that was distributed with this source code.
 */

namespace Kiririn\Webm;

use Kiririn\Webm\Exception\InvalidArgumentException;
use Kiririn\Webm\Exception\NotSupportedException;
use Kiririn\Webm\Exception\CreatePreviewException;
use Kiririn\Webm\Exception\UnsatisfiedDependencyException;

/**
 * Class Webm
 * @package Kiririn\Webm
 */
class Webm {

    /**
     * Path to source file
     *
     * @type string
     */
    private $path;

    /**
     * Path to thumb tmp file
     *
     * @type string
     */
    private $thumbPath;

    /**
     * Thumb file format
     *
     * @type string
     */
    private $format;

    /**
     * @type int
     */
    private $srcW = 0;

    /**
     * @type int
     */
    private $srcH = 0;

    /**
     * @type int
     */
    private $thumbW = 0;

    /**
     * @type int
     */
    private $thumbH = 0;

    /**
     * @type int
     */
    private $duration;


    /**
     * @return int
     */
    public function getWidth() {
        return $this->srcW;
    }


    /**
     * @return int
     */
    public function getHeight() {
        return $this->srcH;
    }


    /**
     * @return int
     */
    public function getThumbWidth() {
        return $this->thumbW;
    }


    /**
     * @return int
     */
    public function getThumbHeight() {
        return $this->thumbH;
    }


    /**
     * @return int
     */
    public function getDuration() {
        if (!$this->duration) {
            $res = shell_exec("mediainfo --Inform='General;%Duration%' ".$this->path);
            $this->duration = intval($res);
        }
        return $this->duration;
    }


    /**
     * Set webm width/height
     */
    private function setVideoSize() {
        $res = shell_exec("mediainfo --Inform='Video;%Width%|%Height%' ".$this->path);
        list($w, $h) = explode('|', trim($res));
        $this->srcW = (int)$w;
        $this->srcH = (int)$h;
    }


    /**
     * Check dependencies
     *
     * @throws UnsatisfiedDependencyException
     */
    private function checkDependencies() {
        $deps = ['ffmpegthumbnailer', 'mediainfo'];
        foreach ($deps as $dep) {
            exec(sprintf("%s 2>/dev/null", $dep), $output, $value);
            if ($value == 127) {
                throw new UnsatisfiedDependencyException(
                    sprintf("%s not found.", $dep)
                );
            }
        }
    }


    /**
     * @param $path
     * @param bool $skipChecking
     */
    public function __construct($path, $skipChecking = false) {
        if (!$skipChecking) {
            $this->checkDependencies();
        }
        if (!is_file($path)) {
            throw new InvalidArgumentException(
                'File not found.'
            );
        }
        $fileInfo = new \finfo(FILEINFO_MIME);
        if (strpos($fileInfo->file($path), 'video/webm') === false) {
            throw new NotSupportedException(
                'Supports only webm video(mime: video/webm).'
            );
        }
        $this->path = $path;
        $this->setVideoSize();
    }


    /**
     * @param int $width
     * @param int $height
     * @param string $format
     * @param int $quality
     */
    public function thumbnail(
        $width = 200,
        $height = 200,
        $format = 'jpeg',
        $quality = 9) {
        if (!is_int($width) ||
            !is_int($height)) {
            throw new InvalidArgumentException(
                'Width/height must be integer.'
            );
        }
        if (!in_array($format, ['jpeg', 'png'])) {
            throw new InvalidArgumentException(
                'Invalid image format(jpeg or png)'
            );
        }
        if (!is_int($quality) ||
            $quality < 0 ||
            $quality > 10) {
            throw new InvalidArgumentException(
                'Quality must be integer(0-10).'
            );
        }

        $this->format = $format;
        $this->thumbPath = tempnam(sys_get_temp_dir(), 'kiri_webm_');

        //shell command
        $shell = 'ffmpegthumbnailer'.
            ' -s '.max($width, $height).    //thumb size
            ' -c '.$this->format.           //image format
            ' -q '.$quality.                //quality(0-10, only jpeg)
            ' -i '.$this->path.             //path to .webm file
            ' -o '.$this->thumbPath.        //path to thumb file
            ' 2>/dev/null';
        exec($shell);

        //check size
        list($this->thumbW, $this->thumbH) = getimagesize($this->thumbPath);
        if ($this->thumbW == 0 ||
            $this->thumbH == 0) {
            unlink($this->thumbPath);
            throw new CreatePreviewException(
                'An error occurred while creating preview.'
            );
        }
    }


    /**
     * @param $path
     * @param $name
     * @throws \Exception
     */
    public function save($path, $name) {
        if (!empty($path)) {
            $path .= (substr($path, -1) == '/' ? '' : '/');
        }
        if (count($n = explode('.', $name)) == 2) {
            list($name, $this->format) = $n;
        }
        if (!rename(
            $this->thumbPath,
            $path.$name.'.'.$this->format)) {
            throw new \Exception(
                'An error occurred while moving preview.'
            );
        }
    }

} 