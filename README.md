## Install
Debian/Ubuntu
as root:
```bash
apt install ffmpegthumbnailer mediainfo
```
## Usage
```php
use Kiririn\Webm\Webm;

//You can skip dependency checking: new Webm('path/to/video.webm', true);
$webm = new Webm('path/to/video.webm');
$webm->thumbnail(200, 200);

//Second argument - thumb name with or without extension
$webm->save('path/to', 'thumb.jpeg');
```

## Get additional info
```php
//Video
$webm->getWidth();
$webm->getHeight();

//Thumb
$webm->getThumbWidth();
$webm->getThumbHeight();

//Duration
$webm->getDuration();
```